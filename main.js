var express = require('express');

var app = express()
    , http = require('http')
    , server = http.createServer(app)
    , io = require('socket.io').listen(server);

var PORT = 8080;

server.listen(PORT, function(){
    console.log('Listening on port '+ PORT);
});

app.configure(function(){
    app.use(express.static(__dirname + '/static'));
});

app.get('/', function(request, response){
    response.render('main.jade');
});

require('./io.js')(io);
